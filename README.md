# Linux sleep battery monitor

### Prelude

My new laptop has stupid problem, that it takes too much power on `S3` power state, so I created this simple script that logs battery level before going to `S3` and after resuming from `S3`


### Install
```
# make install
# systemctl enable sleep-battery-monitor.service
# systemctl enable resume-battery-monitor.service
```

### Data analyzer

Just paste your data file to `data-analyzer.ods` file on `A1` field

It should spit out discharge rate per percent on column `G`
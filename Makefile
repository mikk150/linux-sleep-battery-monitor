install:
	install -D -m 755 monitor-battery /usr/sbin
	install -D -m 644 sleep-battery-monitor.service /lib/systemd/system
	install -D -m 644 resume-battery-monitor.service /lib/systemd/system
	$(MKDIR) /var/lib/sleep-battery-monitor

uninstall:
	$(RM) /usr/sbin/monitor-battery
	$(RM) /lib/systemd/system/sleep-battery-monitor.service
	$(RM) /lib/systemd/system/resume-battery-monitor.service
